'use strict';

const express = require('express');
const router = express.Router();

// for loop
router.get('/test1/for', function(req, res) {
  var arr = [1, 2, 3, 4];
  for (var i = 0, sum = 0; i < arr.length; sum += arr[i++])
  ;
  res.send({result: sum});
});

//while loop
router.get('/test1/while', function(req, res) {
  var arr = [1, 2, 3, 20];
  var sum = 0;
  var i = arr.length;
  while (--i)
    sum += arr[i];
  res.send({result: sum});
});

// recursion
var sum = function(array) {
 if(array.length === 0){
      return 0; // si le tableau est vide
 }
 var val = array.pop();
 if(array.length === 0){
   return val;

 } else {
   var totalSum = sum(array); //
   return val+totalSum;
 }
}

router.get('/test1/recursion', function(req, res) {
  var total = sum([1, 2, 3, 10])
  res.send({result: total });
});

// For each
router.get('/test1/foreach', function(req, res, next) {
  var sum = 0;
  var objPrix = {prix1: 100, prix2: 230, prix3: 400};
  for (var prix in objPrix) {
    sum = sum + objPrix[prix];
  }
  res.send({result: sum });
});

//combines two lists by alternatingly
function newList(alph, num) {
  var list = [];
  for (var i = 0; i < alph.length; ++i) {
    list.push(alph[i]);
    list.push(num[i]);
  }
  return list;
}
router.get('/test2', function(req, res) {
  var listcomb = newList(['A', 'B', 'C'], [1, 2, 3])
  res.send({result: listcomb});
});

router.use("/default", route("default"));

module.exports = router;
